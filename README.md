# Brealtime App

Django App that's been built upon using the heroku [getting started template](https://devcenter.heroku.com/articles/getting-started-with-python)

## Running Locally

Make sure you have Python 3.7 [installed locally](http://install.python-guide.org). 

```sh
$ git clone https://kirankoduru@bitbucket.org/kirankoduru/brealtime.git
$ cd brealtime

$ python3 -m venv brealtime
$ pip install -r requirements.txt

$ export SECRET_KEY='some-random-key'
$ python manage.py runserver
```

Your app should now be running on [localhost:8000](http://localhost:8000/).
