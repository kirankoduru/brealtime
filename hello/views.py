import urllib
import logging

from django.shortcuts import render
from django.http import HttpResponse

logger = logging.getLogger(__name__)

def copy_grid(items):
    return [[j for j in i] for i in items]

def create_grid(text):
    rows = text.split(' ')
    rows = [row[1:] for row in rows]
    items = [[item for item in row] for row in rows]
    return items

def set_equals(items):
    items = copy_grid(items)
    for i, item in enumerate(items):
        for j, item in enumerate(item):
            if i == j:
                items[i][j] = '='
    return items

def set_opp(items):
    items = copy_grid(items)
    for i, _ in enumerate(items):
        for j, _ in enumerate(items):
            if items[i][j] == '>':
                items[j][i] = '<'
            elif items[i][j] == '<':
                items[j][i] = '>'
    return items

def set_one_val(items):
    items = copy_grid(items)
    for item in items:
        fill_in_val = ''.join(item).replace('=','').replace('-', '')
        if len(fill_in_val) == 1:
            for i, v in enumerate(item):
                if v not in [fill_in_val, '=']:
                    item[i] = fill_in_val
    return items

def fill_alt_row_arrows(items):
    items = copy_grid(items)
    count = 2 
    for i, _ in enumerate(items):
        for j, _ in enumerate(items):
            if items[i][j] == '-' and count == 2:
                items[i][j] = '<'
                count -= 1

            if items[i][j] == '-' and count == 1:
                items[i][j] = '>'
                count -= 1
    return items

def solve_puzzle(text):

    grid = create_grid(text)
    grid = set_equals(grid)
    grid = set_opp(grid)
    grid = set_one_val(grid)
    grid = fill_alt_row_arrows(grid)

    headers = [' ', 'A', 'B', 'C', 'D']
    result = [headers] + grid
    str_result = ''
    spaces = ''
    for i, item in enumerate(result):
        if i != 0:
            spaces = headers[i]
        str_result += spaces + ''.join(item) + '\n'
    return str_result



# Create your views here.
def index(request):
    if request.GET.get('q') == 'Degree':
        return HttpResponse('Masters in CS')
    elif request.GET.get('q') == 'Email':
        return HttpResponse('kiranrkoduru@gmail.com')
    elif request.GET.get('q') == 'Name':
        return HttpResponse('Kiran Koduru')
    elif request.GET.get('q') == 'Phone':
        return HttpResponse('347-993-3973')
    elif request.GET.get('q') == 'Position':
        return HttpResponse('DevOps Engineer')
    elif request.GET.get('q') == 'Referrer':
        return HttpResponse('Emily')
    elif request.GET.get('q') == 'Resume':
        return HttpResponse('https://safe-basin-79082.herokuapp.com/static/resume.pdf')
    elif request.GET.get('q') == 'Source':
        return HttpResponse('https://bitbucket.org/kirankoduru/brealtime/src')
    elif request.GET.get('q') == 'Status':
        return HttpResponse('Yes')
    elif request.GET.get('q') == 'Years':
        return HttpResponse('9')
    elif request.GET.get('q') == 'Puzzle':
        request_arg = request.GET.get('d')
        logger.info(request_arg)
        
        puzzle = urllib.parse.unquote_plus(request_arg).strip().split('\n')
        logger.info(puzzle)
        
        puzzle_text = ' '.join(puzzle[2:])
        logger.info(puzzle_text)

        answer = solve_puzzle(puzzle_text)
        logger.info(answer)
        return HttpResponse(answer)

    return HttpResponse('OK')
